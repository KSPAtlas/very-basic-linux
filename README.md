# very-basic-linux
A Linux distribution with the intent of being able to fit everything into the bzImage.

# Compilation (old)
This one is a bit complicated.

1) Acquire a copy of the Linux kernel source code. 
2) Edit the provided .config file to set CONFIG_INITRAMFS_SOURCE to where the folder "root" from the repo is located.
3) Copy said .config file into the root of the Linux source directory.
4) Build the kernel with `make`.
5) The finished image will be found at `<KERNEL SOURCE DIR>/arch/x86/boot/bzImage`.

# Compilation (new)
1) Download the Linux source tree, move it to the repo dir and name it `linux`.
3) Run `make prepare` to remove all the files required to store on Git. (This will also created required device nodes, which requires sudo privileges.)
4) Run `make` to build.

# How to use
THIS IS NOT A DISK IMAGE. You cannot boot it directly. Instead, use a bootloader to load it. No kernel parameters are necessary (But can be used). You can, however,
boot with QEMU using `qemu-system-x86_64 -kernel <PATH_TO_BZIMAGE> <EXTRA_QEMU_PARAMS>`.

# Git etiquette
These are just here so that commits don't mess up things

1) Before adding the files to be commited, use `make gitkeepadd` to add .gitkeep files, to keep the directory structure intact.
2) Only add what is required - like `root` when updating the root filesystem, or `Makefile` when updating the makefile.
3) When you're done, type `make gitkeepremove` to remove all the .gitkeep files.
