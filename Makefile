all:
	mv .config linux/
	cd linux; make
	mv linux/arch/x86/boot/bzImage finished.linux

prepare: gitkeepremove
	chmod +x root/init
	chmod +x root/bin/busybox
	chmod +x root/usr/share/udhcpc/default.script
	cd root/dev; sudo mknod -m 622 console c 5 1
	cd root/dev; sudo mknod -m 622 tty0 c 4 0
	cd root/dev; sudo mknod -m 622 tty1 c 4 0
	cd root/dev; sudo mknod -m 622 tty2 c 4 0
	cd root/dev; sudo mknod -m 622 tty3 c 4 0
	cd root/dev; sudo mknod -m 622 tty4 c 4 0
	echo "Please acquire a copy of the Linux kernel source tree, rename it to linux then run make."

gitkeepremove:
	rm -f $$(find root -name .gitkeep)
	touch root/dev/.gitkeep

gitkeepadd:
	# Preserves directories
	find root -type d -empty -exec touch {}/.gitkeep \;
